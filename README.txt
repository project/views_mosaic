This module create a views style based on Mosaic jQuery plugin to create 
a row with sliding boxes and captions.

Author:
  Rafael Caceres <http://drupal.org/user/412650>


Requirements
------------

1. Views


Installation
------------

1. Place this module directory in your modules folder (usually
sites/all/modules/).

2. Go to "Administer" > "Modules" and enable the module.


Create a Mosaic jQuery view
---------------------------

1. Create views. In format tab set show item as Mosaic jQuery

2. At Format Show Settings, you have to choose the backdrop image and
configure Mosaic jQuery Options.

3. All fields exept the backdrop imagem will be overlayed. You can get inline
fiels just like usual fields formater.

4. Configure CSS styles



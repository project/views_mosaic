<?php
/**
 * @file
 * Implementation of views_row_plugin
 */

class viewsPluginRowViewsMosaic extends views_plugin_row_fields {

  function option_definition() {
    $options = parent::option_definition();
    $options['backdrop'] = array('default' => NULL);
    $options['theme'] = array('default' => 'fade');
    $options['opacity'] = array('default' => 0.5);
    $options['speed'] = array('default' => 150);
    $options['animation'] = array('default' => 'fade');
    $options['anchor_x'] = array('default' => 'left');
    $options['anchor_y'] = array('default' => 'bottom');
    $options['hover_x'] = array('default' => '0px');
    $options['hover_y'] = array('default' => '0px');
    $options['preload'] = array('default' => 1);

    return $options;
  }

  /**
   * Provide a form for setting options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    //Get all fields so user can choose any to be the backdrop
    $options_fields = $this->display->handler->get_field_labels();

    $form['backdrop'] = array(
      '#title' => t('Backdrop'),
      '#type' => 'select',
      '#default_value' => $this->options['backdrop'],
      '#description' => t('Choose the field to be backdrop image. All other fields will be overlayed'),
      '#options' => $options_fields,
    );

    $theme_options = array(
      'custom' => t('Custom'),
      'circle' => t('Circle'),
      'fade' => t('Fade'),
      'bar' => t('Bar'),
      'bar2' => t('Bar with content'),
      'bar3' => t('Top bar'),
      'cover' => t('Cover right'),
      'cover2' => t('Cover top'),
      'cover3' => t('Cover top right')
    );

    $form['theme'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#description' => t('Theme from mosaic jquery examples'),
      '#options' => $theme_options,
      '#default_value' => $this->options['theme'],
    );

    $form['mosaic_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mosaic Options'),
      '#collapsible' => TRUE,
      '#states' => array(
        'visible' => array(
          '#edit-row-options-theme' => array('value' => 'custom'),
        ),
      ),
    );

    //List opacity options from 0.1 to 1 (0.1, 0.2, ..)
    $opacity_options = range(.1, 1, .1); 

    $form['opacity'] = array(
      '#type' => 'select',
      '#title' => t('Opacity'),
      '#description' => t('The final opacity of overlay'),
      '#options' => $opacity_options,
      '#default_value' => $this->options['opacity'],
      '#fieldset' => 'mosaic_options',
    );

    $form['speed'] = array(
      '#type' => 'textfield',
      '#title' => t('Speed'),
      '#description' => t('Speed of animation in milesconds'),
      '#default_value' => $this->options['speed'],
      '#fieldset' => 'mosaic_options',
    );

    $form['animation'] = array(
      '#type' => 'select',
      '#title' => t('Animation type'),
      '#description' => t('Fade or slide mosaic'),
      '#options' => array(
        'fade' => 'Fade',
        'slide' => 'Slide'
      ),
      '#default_value' => $this->options['animation'],
      '#fieldset' => 'mosaic_options',
    );

    $form['anchor_x'] = array(
      '#type' => 'select',
      '#title' => t('Anchor X'),
      '#description' => t('Horizontal anchor points for the overlay'),
      '#options' => array(
        'left' => t('Left'), 
        'right' => t('Right'),
      ),
      '#default_value' => $this->options['anchor_x'],
      '#fieldset' => 'mosaic_options',
    );

    $form['anchor_y'] = array(
      '#type' => 'select',
      '#title' => t('Anchor Y'),
      '#description' => t('Horizontal anchor points for the overlay'),
      '#options' => array(
        'top' => t('Top'),
        'bottom'=> t('Bottom'),
      ),
      '#default_value' => $this->options['anchor_y'],
      '#fieldset' => 'mosaic_options',
    );

    $form['hover_x'] = array(
      '#type' => 'textfield',
      '#title' => t('Hover X'),
      '#description' => t('Horizontal overlay hover positions. Use px or %'),
      '#size' => 10,
      '#maxlength' => 10,
      '#default_value' => $this->options['hover_x'],
      '#fieldset' => 'mosaic_options',
    );
    
    $form['hover_y'] = array(
      '#type' => 'textfield',
      '#title' => t('Hover Y'),
      '#description' => t('Vertical overlay hover positions. Use px or %'),
      '#size' => 10,
      '#maxlength' => 10,
      '#default_value' => $this->options['hover_y'],
      '#fieldset' => 'mosaic_options',
    );

    $form['preload'] = array(
      '#type' => 'checkbox',
      '#title' => t('Preload'),
      '#description' => t('Overlays and backdrops are faded in after the page has loaded'),
      '#default_value' => $this->options['preload'],
      '#fieldset' => 'mosaic_options',
    );

  }
}
